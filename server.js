const path = require("path");
const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const jwt = require("jsonwebtoken");
const passport = require("passport");
const bodyParser = require("body-parser");
const users = require("./users.json");
const texts = require("./texts.json");
const СommentBot = require("./comment-bot.js");

require("./passport.config.js");

server.listen(3000);
app.use(express.static(path.join(__dirname, "public")));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

app.get("/race", (req, res) => {
  res.sendFile(__dirname + "/race.html");
});

app.get("/login", (req, res) => {
  res.sendFile(__dirname + "/login.html");
});

app.post("/login", function(req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, "someSecret", { expiresIn: "24h" });
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

const TIME_TO_START = 15000;
const TIME_TO_GET_JOKE = 11000;
const TIME_TO_GET_STATS = 30000;

const roomname = "gameroom1";
let firstConnectTime = 0;
let startUseTime = 0;
let joinedUsers = [];
let finishedUsers = [];

let timer;
let updater;
let jokes;

const bot = new СommentBot();

// Proxy pattern
const handler = {
  get(target, name) {
    return name in target ? target[name] : null;
  }
};
// Factory pattern
const factoryRequest = (room, action) => ({
  room,
  action,
  send(params, action = undefined) {
    if (action) {
      this.action = action;
    }
    io.in(this.room).emit(this.action, params);
  }
});

// Factory pattern
// can create more rooms
const sendler = factoryRequest(roomname);
const commenter = factoryRequest(roomname, "comment");
//curring
const setTimer = time => func => {
  return setTimeout(func, time);
};

const startGame = () => {
  const text = texts[Math.round(Math.random() * 2)];

  sendler.send({ text }, "play");
  sendler.send({ time: 150 }, "startTimer");

  setTimer(150000)(() => {
    sendler.send({}, "endGame");
    commenter.send({ text: bot.onGameFinish(joinedUsers.slice(0, 3)) });
    setTimer(15000)(() => {
      commenter.send({ text: bot.parting() });
    });
  });
  return new Date().getTime();
};

// facade pattern
// basic logic hide implementation
class Logic {
  constructor(socket) {
    this.socket = socket;
  }

  choosePath(roomLength) {
    if (roomLength === 1) {
      firstConnectTime = new Date().getTime();
      sendler.send({ time: 15 }, "startTimer");

      timer = setTimer(TIME_TO_START)(() => {
        startUseTime = new Date().getTime();
        startGame();
        commenter.send({
          text: bot.getListPlayer(joinedUsers)
        });

        jokes = setInterval(sendJoke, TIME_TO_GET_JOKE);

        updater = setInterval(() => {
          const sortedUsers = joinedUsers.sort(
            (first, second) => second.score - first.score
          );
          commenter.send({
            text: bot.getTime(sortedUsers)
          });
        }, TIME_TO_GET_STATS);
      });
    } else if (roomLength > 1) {
      const anotherConnectTime = new Date().getTime();
      const diff = anotherConnectTime - firstConnectTime;
      const diffBetweenPlayersTime = (TIME_TO_START - diff).toString();

      this.socket.emit("startTimer", {
        time: diffBetweenPlayersTime.slice(0, diffBetweenPlayersTime.length - 3)
      });
    }
  }
}
// currying
const setParamsInArr = arr => params => {
  const idx = arr.findIndex(item => item.login === params.login);
  arr[idx] = { ...params };
};

const deleteUsersFromArr = arr => login => {
  const idx = arr.findIndex(user => user.login === login);
  return [...arr.slice(0, idx), ...arr.slice(idx + 1)];
};

const sendJoke = () => {
  commenter.send({ text: bot.joke() });
};

io.on("connection", socket => {
  let login;
  let score = 0;
  const logic = new Logic(socket);

  socket.on("joinToGame", ({ token }) => {
    socket.join(roomname);

    login = jwt.decode(token).login;
    commenter.send({ text: bot.greeting() });

    const room = io.sockets.adapter.rooms[roomname];
    // proxy pattern
    const user = new Proxy({ login }, handler);
    joinedUsers.push(user);

    io.emit("newPlayer", { login });
    // facade pattern
    logic.choosePath(room.length);

    socket.on("disconnect", () => {
      joinedUsers = deleteUsersFromArr(joinedUsers)(login);
      sendler.send({ login }, "leavePlayer");
      socket.leave(roomname);

      if (room.length === 0) {
        clearTimeout(timer);
      }
    });
  });

  socket.on("correctAnswer", ({ percentage }) => {
    score++;
    const usedTime = new Date().getTime() - startUseTime;
    setParamsInArr(joinedUsers)({ login, score, usedTime });
    sendler.send(
      {
        login,
        score,
        percentage
      },
      "updateScore"
    );
  });

  socket.on("nearFinish", () => {
    commenter.send({ text: bot.onNearFinish(login) });
  });

  socket.on("playerEnd", () => {
    const usedTime = new Date().getTime() - startUseTime;
    setParamsInArr(joinedUsers)({ login, score, usedTime });
    commenter.send({
      text: bot.onPlayerFinish(login)
    });

    const room = io.sockets.adapter.rooms[roomname];
    finishedUsers.push(login);

    if (finishedUsers.length === room.length) {
      clearTimeout(timer);
      clearInterval(updater);
      clearInterval(jokes);

      const sortedUsers = joinedUsers.sort(
        (first, second) => first.usedTime - second.usedTime
      );

      sendler.send({}, "endGame");
      commenter.send({ text: bot.onGameFinish(sortedUsers.slice(0, 3)) });
      setTimer(15000)(() => {
        commenter.send({ text: bot.parting() });
      });
    }
  });
});
